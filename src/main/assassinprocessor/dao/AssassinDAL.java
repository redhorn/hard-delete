package dao;

import java.io.Closeable;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.SetMultimap;

/**
 * 
 * 
 * @author pkandasamy
 * 
 */
public interface AssassinDAL extends Closeable {
	
	Set<String> getAllDataSourceNamesToDelete() throws SQLException;

	Set<String> getAllDataSourceNamesToHide() throws SQLException;

	Set<DataSourceDeleteSubmission> getAllDSDeleteSubmissionsToDelete() throws SQLException;

	Set<DataSourceDeleteSubmission> getAllDSDeleteSubmissionsToHide() throws SQLException;

	DataSourceDeleteSubmission getDSDeleteSubmissionById(Long id) throws SQLException;

	Set<DataSourceDeleteSubmission> getDSDeleteSubmissionsByDataSourceName(String dsName) throws SQLException;

	Set<Long> getAllObjectIdsToDelete() throws SQLException;

	Set<Long> getAllObjectIdsToHide() throws SQLException;

	Set<ObjectDeleteSubmission> getAllObjectSubmissionsToDelete() throws SQLException;

	Set<ObjectDeleteSubmission> getAllObjectSubmissionsToHide() throws SQLException;

	ObjectDeleteSubmission getObjectSubmissionById(Long id) throws SQLException;

	Set<ObjectDeleteSubmission> getObjectSubmissionsByObjectId(Long objectId) throws SQLException;

	SetMultimap<String, String> getAllPropertiesToDelete() throws SQLException;

	SetMultimap<String, String> getAllPropertiesToHide() throws SQLException;

	Set<PropertyDeleteSubmission> getAllPropertyDeleteSubmissionsToDelete() throws SQLException;

	Set<PropertyDeleteSubmission> getAllPropertyDeleteSubmissionsToHide() throws SQLException;

	PropertyDeleteSubmission getPropertyDeleteSubmissionById(Long id) throws SQLException;

	Set<PropertyDeleteSubmission> getPropertyDeleteSubmissionsByUriAndValue(String propertyUri, String propertyValue) throws SQLException;

	int updateObjectHideStatus(Set<Long> objectIdsToUpdate, boolean flag) throws SQLException;

	int updateDataSourceHideStatus(Set<String> dataSourceNamesToUpdate, boolean flag) throws SQLException;

	int updatePropertyHideStatus(Map<String, Collection<String>> propertyMap, boolean flag) throws SQLException;

	int createDataSourceSubmission(String dsName, String sourceSystemName) throws SQLException;
	
	int createObjectSubmission(Long objectId, String sourceSystemName) throws SQLException;
	
	int createPropertySubmission(String propertyUri, String propertyValue, String sourceSystemName) throws SQLException;

	int updatePropertyDeleteStatus(Map<String, Collection<String>> propertyMap, boolean flag) throws SQLException;
	
	int updateObjectDeleteStatus(Set<Long> objectIdsToUpdate, boolean flag) throws SQLException;

	int updateDataSourceDeleteStatus(Set<String> dataSourceNamesToUpdate, boolean flag) throws SQLException;

}

package dao;

public final class AssassinDALConstants {
	public static final String SQL_CONNECTION = "sqldelete.connectionstring";
	
	public final class DataSourceColumns {
		public static final String DS_SUBMISSION_ID = "DsDeleteSubmissionID";
		public static final String DS_NAME = "DsName";
		private DataSourceColumns() {
			
		}
	}
	
	public final class ObjectColumns {
		public static final String OBJECT_SUBMISSION_ID = "ObjectDeleteSubmissionID";
		public static final String OBJECT_ID = "ObjectId";
		/**
		 * Private constructor to prevent instantiation.
		 */
		private ObjectColumns() {
			
		}
	}
	
	public final class PropertyColumns {
		public static final String PROPERTY_SUBMISSION_ID = "PropertyDeleteSubmissionID";
		public static final String PROPERTY_URI = "PropertyURI";
		public static final String PROPERTY_VALUE = "PropertyValue";
		/**
		 * Private constructor to prevent instantiation.
		 */
		private PropertyColumns() {
			
		}
	}
	
	public final class CommonColumns {
		public static final String SOURCE_SYSTEM = "SourceSystemName";
		public static final String HIDE_FLAG = "HideStatusFlag";
		public static final String SUCCESS_FLAG = "SuccessFlag";
		public static final String RETRY_COUNT = "RetryCount";
		/**
		 * Private constructor to prevent instantiation.
		 */
		private CommonColumns() {
			
		}
	}
	
	/**
	 * Private constructor to prevent instantiation.
	 */
	private AssassinDALConstants() {
	
	}
}

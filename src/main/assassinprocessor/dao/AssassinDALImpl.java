package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

/**
 * DAL used by Assassin to read, write and update records in the SQL database.
 * 
 * @author pkandasamy
 */
public class AssassinDALImpl implements AssassinDAL {
	private static final Logger LOG = LoggerFactory.getLogger(AssassinDALImpl.class);
	
	/**
	 * Sql delete database connection.
	 */
	private Connection deleteDbConnection;
	
	/**
	 * Constructor.
	 * 
	 * @throws SQLException 
	 */
	public AssassinDALImpl(Properties properties) throws SQLException {
		this.deleteDbConnection = initializeDeleteDbConnection(properties);
	}
	
	/**
	 * Initialize the connection to the sql delete database.
	 * 
	 * @param deleteDbConnectionString
	 * 			The connection string to the sql delete database for the jdbc connection.
	 * @return
	 * 			The sql delete database connection.
	 * @throws SQLException
	 * 			If there is an error connecting to the sql delete database.
	 */
	private Connection initializeDeleteDbConnection(Properties properties) throws SQLException {
		return DriverManager.getConnection(properties.getProperty(AssassinDALConstants.SQL_CONNECTION));
	}
	
	private Set<String> getSetOfDataSourceNames(PreparedStatement psDataSourceNames) throws SQLException {
		Set<String> setOfUniqueDataSourceNames = new HashSet<String>();
		try (ResultSet rsDataSources = psDataSourceNames.executeQuery()) {
			while (rsDataSources.next()) {
				String dataSourceName = rsDataSources.getString(AssassinDALConstants.DataSourceColumns.DS_NAME);
				setOfUniqueDataSourceNames.add(dataSourceName);
			}
		}
		
		return setOfUniqueDataSourceNames;
	}

	private Set<DataSourceDeleteSubmission> getSetOfDataSourceSubmissions(PreparedStatement psDataSourceNames) throws SQLException {
		Set<DataSourceDeleteSubmission> setOfUniqueDataSourceSubmissions = new HashSet<DataSourceDeleteSubmission>();
		try (ResultSet rsDataSources = psDataSourceNames.executeQuery()) {
			while (rsDataSources.next()) {
				DataSourceDeleteSubmission dsSubmission = new DataSourceDeleteSubmission(rsDataSources);
				setOfUniqueDataSourceSubmissions.add(dsSubmission);
			}
		}
		return setOfUniqueDataSourceSubmissions;
	}

	private Set<Long> getSetOfObjectIds(PreparedStatement psObjectIds) throws SQLException {
		Set<Long> setOfUniqueObjectIds = new HashSet<Long>();
		try (ResultSet rsObjectIds = psObjectIds.executeQuery()) {
			while (rsObjectIds.next()) {
				Long objectId = rsObjectIds.getLong(AssassinDALConstants.ObjectColumns.OBJECT_ID);
				setOfUniqueObjectIds.add(objectId);
			}
		}	
		return setOfUniqueObjectIds;
	}
	
	private Set<ObjectDeleteSubmission> getSetOfObjectDeleteSubmissions(PreparedStatement psObjectIds) throws SQLException {
		Set<ObjectDeleteSubmission> setOfUniqueObjectSubmissions = new HashSet<ObjectDeleteSubmission>();
		try (ResultSet rsObjectIds = psObjectIds.executeQuery()) {	
			while (rsObjectIds.next()) {
				ObjectDeleteSubmission objectDeleteSubmission = new ObjectDeleteSubmission(rsObjectIds);
				setOfUniqueObjectSubmissions.add(objectDeleteSubmission);
			}
		}
		return setOfUniqueObjectSubmissions;
	}
	
	private Set<PropertyDeleteSubmission> getSetOfPropertyDeleteSubmissions(PreparedStatement psProperties) throws SQLException {
		Set<PropertyDeleteSubmission> setOfUniquePropertySubmissions = new HashSet<PropertyDeleteSubmission>();
		try (ResultSet rsProperties = psProperties.executeQuery()) {
			while (rsProperties.next()) {
				PropertyDeleteSubmission propertyDeleteSubmission = new PropertyDeleteSubmission(rsProperties);
				setOfUniquePropertySubmissions.add(propertyDeleteSubmission);
			}
		}
		return setOfUniquePropertySubmissions;
	}
	
	private SetMultimap<String, String> getMapOfProperties(PreparedStatement psProperties) throws SQLException {
		SetMultimap<String, String> propertyMap = HashMultimap.create();
		try (ResultSet rsProperties = psProperties.executeQuery()) {
			while (rsProperties.next()) {
				String propertyUri = rsProperties.getString(AssassinDALConstants.PropertyColumns.PROPERTY_URI);
				String propertyValue = rsProperties.getString(AssassinDALConstants.PropertyColumns.PROPERTY_VALUE);
				propertyMap.put(propertyUri, propertyValue);
			}
		}
		return propertyMap;
	}
	
	@Override
	public Set<String> getAllDataSourceNamesToDelete() throws SQLException {
		try (PreparedStatement selectDataSourceNamesToDelete = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE SuccessFlag='False'")) {
			return getSetOfDataSourceNames(selectDataSourceNamesToDelete);
		}
	}

	@Override
	public Set<String> getAllDataSourceNamesToHide() throws SQLException {
		try (PreparedStatement selectDataSourceNamesToHide = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getSetOfDataSourceNames(selectDataSourceNamesToHide);
		}
	}

	@Override
	public Set<DataSourceDeleteSubmission> getAllDSDeleteSubmissionsToDelete() throws SQLException {
		try (PreparedStatement selectDataSourceSubmissionsToDelete = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE SuccessFlag='False'")) {
			return getSetOfDataSourceSubmissions(selectDataSourceSubmissionsToDelete);
		}
	}

	@Override
	public Set<DataSourceDeleteSubmission> getAllDSDeleteSubmissionsToHide() throws SQLException {
		try (PreparedStatement selectDataSourceSubmissionsToHide = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getSetOfDataSourceSubmissions(selectDataSourceSubmissionsToHide);
		}
	}

	@Override
	public DataSourceDeleteSubmission getDSDeleteSubmissionById(Long id) throws SQLException {
		try (PreparedStatement selectDataSourceSubmissionsById = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE DsDeleteSubmissionID=" + id)) {
			try (ResultSet rsDataSources = selectDataSourceSubmissionsById.executeQuery()) {
				if (rsDataSources.first()) {
					return new DataSourceDeleteSubmission(rsDataSources);
				}
			}
		}
		return null;
	}

	@Override
	public Set<DataSourceDeleteSubmission> getDSDeleteSubmissionsByDataSourceName(String dsName) throws SQLException {
		try (PreparedStatement selectDataSourceSubmissionsByName = deleteDbConnection.prepareStatement("SELECT * FROM DataSourceDeleteSubmission WHERE DsName like %" + dsName +"%")) {
			return getSetOfDataSourceSubmissions(selectDataSourceSubmissionsByName);
		}
	}

	@Override
	public Set<Long> getAllObjectIdsToDelete() throws SQLException {
		try (PreparedStatement selectObjectsToDelete = deleteDbConnection.prepareStatement("SELECT ObjectId FROM ObjectDeleteSubmission WHERE SuccessFlag='False'")) {
			return getSetOfObjectIds(selectObjectsToDelete);
		}
	}
	
	@Override
	public Set<Long> getAllObjectIdsToHide() throws SQLException {
		try (PreparedStatement selectObjectsToHide = deleteDbConnection.prepareStatement("SELECT * FROM ObjectDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getSetOfObjectIds(selectObjectsToHide);
		}
	}
	
	@Override
	public Set<ObjectDeleteSubmission> getAllObjectSubmissionsToDelete() throws SQLException {
		try (PreparedStatement selectObjectsToDelete = deleteDbConnection.prepareStatement("SELECT * FROM ObjectDeleteSubmission WHERE SuccessFlag='False'")) {
			return getSetOfObjectDeleteSubmissions(selectObjectsToDelete);
		}
	}
	
	@Override
	public Set<ObjectDeleteSubmission> getAllObjectSubmissionsToHide() throws SQLException {
		try (PreparedStatement selectObjectsToHide = deleteDbConnection.prepareStatement("SELECT * FROM ObjectDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getSetOfObjectDeleteSubmissions(selectObjectsToHide);
		}
	}

	@Override
	public ObjectDeleteSubmission getObjectSubmissionById(Long id) throws SQLException {
		try (PreparedStatement selectObjectSubmissionsById = deleteDbConnection.prepareStatement("SELECT * FROM ObjectDeleteSubmission WHERE ObjectDeleteSubmissionID=" + id)) {
			try (ResultSet rsObjectIds = selectObjectSubmissionsById.executeQuery()) {
				if (rsObjectIds.first()) {
					return new ObjectDeleteSubmission(rsObjectIds);
				}
			}
		}
		return null;
	}
	
	@Override
	public Set<ObjectDeleteSubmission> getObjectSubmissionsByObjectId(Long objectId) throws SQLException {
		try (PreparedStatement selectObjectSubmissionsByObjectId = deleteDbConnection.prepareStatement("SELECT * FROM ObjectDeleteSubmission WHERE ObjectId=" + objectId)) {
			return getSetOfObjectDeleteSubmissions(selectObjectSubmissionsByObjectId);
		}
	}

	@Override
	public SetMultimap<String, String> getAllPropertiesToDelete() throws SQLException {
		try (PreparedStatement selectPropertyValuesToDelete = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE SuccessFlag='False'")) {
			return getMapOfProperties(selectPropertyValuesToDelete);
		}
	}

	@Override
	public SetMultimap<String, String> getAllPropertiesToHide() throws SQLException {
		try (PreparedStatement selectPropertyValuesToHide = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getMapOfProperties(selectPropertyValuesToHide);
		}
	}

	@Override
	public Set<PropertyDeleteSubmission> getAllPropertyDeleteSubmissionsToDelete() throws SQLException {
		try (PreparedStatement selectPropertySubmissionsToDelete = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE SuccessFlag='False'")) {
			return getSetOfPropertyDeleteSubmissions(selectPropertySubmissionsToDelete);
		}
	}

	@Override
	public Set<PropertyDeleteSubmission> getAllPropertyDeleteSubmissionsToHide() throws SQLException {
		try (PreparedStatement selectPropertySubmissionsToHide = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE HideStatusFlag='False'")) {
			return getSetOfPropertyDeleteSubmissions(selectPropertySubmissionsToHide);
		}
	}

	@Override
	public PropertyDeleteSubmission getPropertyDeleteSubmissionById(Long id) throws SQLException {
		try (PreparedStatement selectDataSourceSubmissionsById = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE PropertyDeleteSubmissionID=" + id)) {
			try (ResultSet rsDataSources = selectDataSourceSubmissionsById.executeQuery()) {
				if (rsDataSources.first()) {
					return new PropertyDeleteSubmission(rsDataSources);
				}
			}
		}
		return null;
	}

	@Override
	public Set<PropertyDeleteSubmission> getPropertyDeleteSubmissionsByUriAndValue(String propertyUri, String propertyValue) throws SQLException {
		try (PreparedStatement selectPropertySubmissionsByUriAndValue = deleteDbConnection.prepareStatement("SELECT * FROM PropertyDeleteSubmission WHERE PropertyURI like %" + propertyUri +"% AND PropertyValue like %" + propertyValue + "%")) {
			return getSetOfPropertyDeleteSubmissions(selectPropertySubmissionsByUriAndValue);
		}
	}
	
	@Override
	public int updatePropertyHideStatus(Map<String, Collection<String>> propertyMap, boolean flag) throws SQLException {
		if ((propertyMap == null) || (propertyMap.isEmpty())) {
			return 0;
		}
		
		int totalRecordsUpdated = 0;
		
		for (Entry<String, Collection<String>> propertyEntry : propertyMap.entrySet()) {
			Set<String> setOfPropertyValues = new HashSet<String>();
			setOfPropertyValues.addAll(propertyEntry.getValue());
			String propertyValueString = "('" + StringUtils.join(setOfPropertyValues, "','") + "')";
			
			try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[ObjectDeleteSubmission] SET [HideStatusFlag]='" + flag + "',[RetryCount] = RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE PropertyURI like %" + propertyEntry + "% AND PropertyValue in " + propertyValueString)) {
				totalRecordsUpdated += psUpdateObjectIdFlag.executeUpdate();
			}
		}
		return totalRecordsUpdated;	
	}
	
	@Override
	public int updatePropertyDeleteStatus(Map<String, Collection<String>> propertyMap, boolean flag) throws SQLException {
		if ((propertyMap == null) || (propertyMap.isEmpty())) {
			return 0;
		}
		
		int totalRecordsUpdated = 0;
		
		for (Entry<String, Collection<String>> propertyEntry : propertyMap.entrySet()) {
			Set<String> setOfPropertyValues = new HashSet<String>();
			setOfPropertyValues.addAll(propertyEntry.getValue());
			String propertyValueString = "('" + StringUtils.join(setOfPropertyValues, "','") + "')";
			
			try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[ObjectDeleteSubmission] SET [SuccessFlag]='" + flag + "',[RetryCount] = RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE PropertyURI like %" + propertyEntry + "% AND PropertyValue in " + propertyValueString)) {
				totalRecordsUpdated += psUpdateObjectIdFlag.executeUpdate();
			}
		}
		return totalRecordsUpdated;	
	}

	@Override
	public int updateObjectHideStatus(Set<Long> objectIdsToUpdate, boolean flag) throws SQLException {	
		if ((objectIdsToUpdate == null) || (objectIdsToUpdate.isEmpty())) {
			return 0;
		}
		
		String objectIdsString = "('" + StringUtils.join(objectIdsToUpdate, "','") + "')";
		try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[ObjectDeleteSubmission] SET [HideStatusFlag]='" + flag + "',[RetryCount]=RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE ObjectId in " + objectIdsString)) {
			return psUpdateObjectIdFlag.executeUpdate();
		}
	}
	
	@Override
	public int updateObjectDeleteStatus(Set<Long> objectIdsToUpdate, boolean flag) throws SQLException {	
		if ((objectIdsToUpdate == null) || (objectIdsToUpdate.isEmpty())) {
			return 0;
		}
		
		String objectIdsString = "('" + StringUtils.join(objectIdsToUpdate, "','") + "')";
		try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[ObjectDeleteSubmission] SET [SuccessFlag]='" + flag + "',[RetryCount]=RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE ObjectId in " + objectIdsString)) {
			return psUpdateObjectIdFlag.executeUpdate();
		}
	}
	
	@Override
	public int updateDataSourceHideStatus(Set<String> dataSourceNamesToUpdate, boolean flag) throws SQLException {	
		if ((dataSourceNamesToUpdate == null) || (dataSourceNamesToUpdate.isEmpty())) {
			return 0;
		}
		
		String dataSourceNamesString = "('" + StringUtils.join(dataSourceNamesToUpdate, "','") + "')";
		try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[DataSourceDeleteSubmission] SET [HideStatusFlag]='" + flag + "',[RetryCount]=RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE DsName in " + dataSourceNamesString)) {
			return psUpdateObjectIdFlag.executeUpdate();
		}			
	}
	
	@Override
	public int updateDataSourceDeleteStatus(Set<String> dataSourceNamesToUpdate, boolean flag) throws SQLException {	
		if ((dataSourceNamesToUpdate == null) || (dataSourceNamesToUpdate.isEmpty())) {
			return 0;
		}
		
		String dataSourceNamesString = "('" + StringUtils.join(dataSourceNamesToUpdate, "','") + "')";
		try (PreparedStatement psUpdateObjectIdFlag = deleteDbConnection.prepareStatement("UPDATE [dbo].[DataSourceDeleteSubmission] SET [SuccessFlag]='" + flag + "',[RetryCount]=RetryCount+1, [LastUpdatedDate] = GETDATE(), [LastUpdatedBy] = 'DeleteUser' WHERE DsName in " + dataSourceNamesString)) {
			return psUpdateObjectIdFlag.executeUpdate();
		}			
	}
	
	@Override
	public int createDataSourceSubmission(String dsName, String sourceSystemName) throws SQLException {
		try (PreparedStatement psCreateDSSubmission = deleteDbConnection.prepareStatement("INSERT INTO [dbo].[DataSourceDeleteSubmission] ([DsName], [SourceSystemName], [HideStatusFlag], [SuccessFlag], [RetryCount], [CreationDate], [LastUpdatedDate], [LastUpdatedBy]) VALUES ('" + dsName + "', '" + sourceSystemName + "', 'false', 'false', 0, GETDATE(), GETDATE(), 'AutomatedTest');")) {
			return psCreateDSSubmission.executeUpdate();
		}
	}

	@Override
	public int createObjectSubmission(Long objectId, String sourceSystemName) throws SQLException {
		try (PreparedStatement psCreateObjectSubmission = deleteDbConnection.prepareStatement("INSERT INTO [dbo].[ObjectDeleteSubmission] ([ObjectId], [SourceSystemName], [HideStatusFlag], [SuccessFlag], [RetryCount], [CreationDate], [LastUpdatedDate], [LastUpdatedBy]) VALUES ('" + objectId + "', '" + sourceSystemName + "', 'false', 'false', 0, GETDATE(), GETDATE(), 'AutomatedTest');")) {
			return psCreateObjectSubmission.executeUpdate();
		}
	}

	@Override
	public int createPropertySubmission(String propertyUri, String propertyValue, String sourceSystemName) throws SQLException {
		try (PreparedStatement psCreatePropertySubmission = deleteDbConnection.prepareStatement("INSERT INTO [dbo].[PropertyDeleteSubmission] ([PropertyURI], [PropertyValue], [SourceSystemName], [HideStatusFlag], [SuccessFlag], [RetryCount], [CreationDate], [LastUpdatedDate], [LastUpdatedBy]) VALUES ('" + propertyUri + "', '" + propertyValue + "', '" + sourceSystemName + "', 'false', 'false', 0, GETDATE(), GETDATE(), 'AutomatedTest');")) {
			return psCreatePropertySubmission.executeUpdate();
		}
	}
	
	public void close() {
		if (deleteDbConnection != null) {
			try {
				deleteDbConnection.close();
			} catch (SQLException e) {
				LOG.error("Error closing delete db connection.", e);
			}
		}
	}
}
package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Get a DataSourceDeleteSubmission for each row of the sql DataSourceDeleteSubmission table.
 * 
 * @author pkandasamy
 * 
 */
public class DataSourceDeleteSubmission extends DeleteSubmission {
	/**
	 * DataSourceDeleteSubmission id.
	 */
	private Long dsDeleteSubmissionId;
	/**
	 * Data source name.
	 */
	private String dsName;
	
	/**
	 * Default empty constructor.
	 */
	public DataSourceDeleteSubmission() {
		
	}
	/**
	 * Constructor with a result set (row) parses out the DataSourceDeleteSubmission properties.
	 * 
	 * @param rsDataSourceSubmission
	 * 		A resultset containing the rows of the DataSourceDeleteSubmission record (row).
	 * @throws SQLException
	 * 		If there is an error parsing our the specified values from the given resultset.
	 */
	public DataSourceDeleteSubmission(ResultSet rsDataSourceSubmission) throws SQLException {
		this.dsDeleteSubmissionId = rsDataSourceSubmission.getLong(AssassinDALConstants.DataSourceColumns.DS_SUBMISSION_ID);
		this.dsName = rsDataSourceSubmission.getString(AssassinDALConstants.DataSourceColumns.DS_NAME);
		this.sourceSystemName = rsDataSourceSubmission.getString(AssassinDALConstants.CommonColumns.SOURCE_SYSTEM);
		this.hideStatusFlag = rsDataSourceSubmission.getBoolean(AssassinDALConstants.CommonColumns.HIDE_FLAG);
		this.successFlag = rsDataSourceSubmission.getBoolean(AssassinDALConstants.CommonColumns.SUCCESS_FLAG);
		this.retryCount = rsDataSourceSubmission.getInt(AssassinDALConstants.CommonColumns.RETRY_COUNT);
	}

	/**
	 * Get the DsDeleteSubmissionId.
	 * 
	 * @return
	 * 		The DsDeleteSubmissionId (Data source delete submission id).
	 */
	public Long getDsDeleteSubmissionId() {
		return dsDeleteSubmissionId;
	}
	/**
	 * Set the DsDeleteSubmissionId.
	 * 
	 * @param dsDeleteSubmissionId
	 * 		The DsDeleteSubmissionId to set to the object property (Data source delete submission id). 
	 */
	public void setDsDeleteSubmissionId(Long dsDeleteSubmissionId) {
		this.dsDeleteSubmissionId = dsDeleteSubmissionId;
	}

	/**
	 * Get the DsName.
	 * 
	 * @return 
	 * 		The dsName (Data source name).
	 */
	public String getDsName() {
		return dsName;
	}
	/**
	 * Set the DsName.
	 * 
	 * @param dsName 
	 * 		The dsName to set.
	 */
	public void setDsName(String dsName) {
		this.dsName = dsName;
	}
}

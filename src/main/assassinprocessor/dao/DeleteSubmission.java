package dao;

/**
 * Common properties of a DeleteSubmission record.
 * 
 * @author pkandasamy
 * 
 */
public class DeleteSubmission {
	/**
	 * Source system name.
	 */
	protected String sourceSystemName;
	/**
	 * Hide status flag.
	 */
	protected boolean hideStatusFlag;
	/**
	 * Success flag.
	 */
	protected boolean successFlag;
	/**
	 * Retry count.
	 */
	protected int retryCount;
	
	/**
	 * Default empty constructor.
	 */
	public DeleteSubmission() {
		
	}
	
	/**
	 * Get the source system name.
	 * 
	 * @return 
	 *			The sourceSystemName.
	 */
	public String getSourceSystemName() {
		return sourceSystemName;
	}
	/**
	 * Set the source system name.
	 * 
	 * @param sourceSystemName 
	 * 			The sourceSystemName to set.
	 */
	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}
	
	/**
	 * Get the hide status flag.
	 * 
	 * @return 
	 * 			The hideStatusFlag
	 */
	public boolean getHideStatusFlag() {
		return hideStatusFlag;
	}
	/**
	 * Set the hide status flag.
	 * 
	 * @param hideStatusFlag 
	 * 			The hideStatusFlag to set.
	 */
	public void setHideStatusFlag(boolean hideStatusFlag) {
		this.hideStatusFlag = hideStatusFlag;
	}
	
	/**
	 * Get the success flag.
	 * 
	 * @return 
	 * 			The successFlag.
	 */
	public boolean getSuccessFlag() {
		return successFlag;
	}
	/**
	 * Set the success flag.
	 * 
	 * @param successFlag 
	 * 			The successFlag to set.
	 */
	public void setSuccessFlag(boolean successFlag) {
		this.successFlag = successFlag;
	}
	
	/**
	 * Get the retry count.
	 * 
	 * @return 
	 * 			The retryCount.
	 */
	public int getRetryCount() {
		return retryCount;
	}
	/**
	 * Set the retry count.
	 * 
	 * @param retryCount 
	 * 			The retryCount to set.
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
}

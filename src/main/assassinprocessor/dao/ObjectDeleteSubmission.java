package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Get a ObjectDeleteSubmission for each row of the sql ObjectDeleteSubmission table.
 * 
 * @author pkandasamy
 *
 */
public class ObjectDeleteSubmission extends DeleteSubmission {
	/**
	 * Object delete submission id.
	 */
	private Long objectDeleteSubmissionId;
	/**
	 * Object id.
	 */
	private Long objectId;

	/**
	 * Default empty constructor.
	 */
	public ObjectDeleteSubmission() {
		
	}
	/**
	 * Constructor with a result set (row) parses out the ObjectDeleteSubmission properties.
	 * 
	 * @param rsObjectDeleteSubmission
	 * 		A resultset containing the rows of the ObjectDeleteSubmission table.
	 * @throws SQLException
	 * 		If there is an error getting the values from the resultset.
	 */
	public ObjectDeleteSubmission(ResultSet rsObjectDeleteSubmission) throws SQLException {
		this.objectDeleteSubmissionId = rsObjectDeleteSubmission.getLong(AssassinDALConstants.ObjectColumns.OBJECT_SUBMISSION_ID);
		this.objectId = rsObjectDeleteSubmission.getLong(AssassinDALConstants.ObjectColumns.OBJECT_ID);
		this.sourceSystemName = rsObjectDeleteSubmission.getString(AssassinDALConstants.CommonColumns.SOURCE_SYSTEM);
		this.hideStatusFlag = rsObjectDeleteSubmission.getBoolean(AssassinDALConstants.CommonColumns.HIDE_FLAG);
		this.successFlag = rsObjectDeleteSubmission.getBoolean(AssassinDALConstants.CommonColumns.SUCCESS_FLAG);
		this.retryCount = rsObjectDeleteSubmission.getInt(AssassinDALConstants.CommonColumns.RETRY_COUNT);
	}

	/**
	 * Get the ObjectDeleteSubmissionId.
	 * 
	 * @return 
	 * 		The objectDeleteSubmissionId.
	 */
	public Long getObjectDeleteSubmissionId() {
		return objectDeleteSubmissionId;
	}
	/**
	 * Set the ObjectDeleteSubmissionId.
	 * 
	 * @param objectDeleteSubmissionId
	 * 		The objectDeleteSubmissionId to set.
	 */
	public void setObjectDeleteSubmissionId(Long objectDeleteSubmissionId) {
		this.objectDeleteSubmissionId = objectDeleteSubmissionId;
	}

	/**
	 * Get the ObjectId.
	 * 
	 * @return 
	 * 		The objectId.
	 */
	public Long getObjectId() {
		return objectId;
	}
	/**
	 * Set the ObjectId.
	 * 
	 * @param objectId 
	 * 		The objectId to set.
	 */
	public void setObjectId(Long objectId) {
		this.objectId = objectId;
	}
}

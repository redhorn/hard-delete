package dao;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Get a PropertyDeleteSubmission for each row of the sql PropertyDeleteSubmission table.
 * 
 * @author pkandasamy
 * 
 */
public class PropertyDeleteSubmission extends DeleteSubmission {
	/**
	 * PropertyDeleteSubmissionId.
	 */
	private Long propertyDeleteSubmissionId;
	/**
	 * PropertyURI.
	 */
	private String propertyUri;
	/**
	 * Property Value.
	 */
	private String propertyValue;
	
	/**
	 * Default empty constructor.
	 */
	public PropertyDeleteSubmission() {
		
	}
	/**
	 * Constructor with a result set (row) parses out the PropertyDeleteSubmission properties.
	 * 
	 * @param rsPropertyDeleteSubmission
	 * 		A resultset containing the rows of the PropertyDeleteSubmission table.
	 * @throws SQLException
	 * 		If there is an error getting the values from the resultset.
	 */
	public PropertyDeleteSubmission(ResultSet rsPropertyDeleteSubmission) throws SQLException {
		this.propertyDeleteSubmissionId = rsPropertyDeleteSubmission.getLong(AssassinDALConstants.PropertyColumns.PROPERTY_SUBMISSION_ID);
		this.propertyUri = rsPropertyDeleteSubmission.getString(AssassinDALConstants.PropertyColumns.PROPERTY_URI);
		this.propertyValue = rsPropertyDeleteSubmission.getString(AssassinDALConstants.PropertyColumns.PROPERTY_VALUE);
		this.sourceSystemName = rsPropertyDeleteSubmission.getString(AssassinDALConstants.CommonColumns.SOURCE_SYSTEM);
		this.hideStatusFlag = rsPropertyDeleteSubmission.getBoolean(AssassinDALConstants.CommonColumns.HIDE_FLAG);
		this.successFlag = rsPropertyDeleteSubmission.getBoolean(AssassinDALConstants.CommonColumns.SUCCESS_FLAG);
		this.retryCount = rsPropertyDeleteSubmission.getInt(AssassinDALConstants.CommonColumns.RETRY_COUNT);
	}

	/**
	 * Get the PropertyDeleteSubmissionId.
	 * 
	 * @return
	 * 		The PropertyDeleteSubmissionId.
	 */
	public Long getPropertyDeleteSubmissionId() {
		return propertyDeleteSubmissionId;
	}
	/**
	 * Set the PropertyDeleteSubmissionId.
	 * 
	 * @param propertyDeleteSubmissionId
	 * 		The propertyDeleteSubmissionId to set.
	 */
	public void setPropertyDeleteSubmissionId(Long propertyDeleteSubmissionId) {
		this.propertyDeleteSubmissionId = propertyDeleteSubmissionId;
	}

	/**
	 * Get the PropertyURI.
	 * 
	 * @return
	 * 		The propertyDeleteSubmissionId.
	 */
	public String getPropertyUri() {
		return propertyUri;
	}
	/**
	 * Set the PropertyURI.
	 * 
	 * @param propertyUri
	 * 		The propertyUri to set.
	 */
	public void setPropertyUri(String propertyUri) {
		this.propertyUri = propertyUri;
	}
	
	/**
	 * Get the propertyValue.
	 * 
	 * @return
	 * 		The propertyValue.
	 */
	public String getPropertyValue() {
		return propertyValue;
	}
	/**
	 * Set the propertyValue.
	 * 
	 * @param propertyValue
	 * 		The propertyValue to set.
	 */
	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}

	/**
	 * Get the sourceSystemName.
	 * 
	 * @return
	 * 		The sourceSystemName.
	 */
	public String getSourceSystemName() {
		return sourceSystemName;
	}
	/**
	 * Set the sourceSystemName.
	 * 
	 * @param sourceSystemName
	 * 		The sourceSystemName to set.
	 */
	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}

	/**
	 * Get the hideStatusFlag.
	 * 
	 * @return
	 * 		The hideStatusFlag.
	 */
	public boolean getHideStatusFlag() {
		return hideStatusFlag;
	}
	/**
	 * Set the hideStatusFlag.
	 * 
	 * @param hideStatusFlag
	 * 		The hideStatusFlag to set.
	 */
	public void setHideStatusFlag(boolean hideStatusFlag) {
		this.hideStatusFlag = hideStatusFlag;
	}

	/**
	 * Get the successFlag.
	 * 
	 * @return
	 * 		The successFlag.
	 */
	public boolean getSuccessFlag() {
		return successFlag;
	}
	/**
	 * Set the successFlag.
	 * 
	 * @param successFlag
	 * 		The successFlag to set.
	 */
	public void setSuccessFlag(boolean successFlag) {
		this.successFlag = successFlag;
	}

	/**
	 * Get the retry count.
	 * 
	 * @return
	 * 		The retry count.
	 */
	public int getRetryCount() {
		return retryCount;
	}
	/**
	 * Set the retry count.
	 * 
	 * @param retryCount
	 * 		The retryCount to set.
	 */
	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}
}

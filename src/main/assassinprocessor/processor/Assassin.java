package processor;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;
import com.palantir.caribou.blade.api.request.DataSourceNameRequest;
import com.palantir.caribou.blade.api.request.ObjectIdRequest;
import com.palantir.caribou.blade.api.request.PropertyRequest;
import com.palantir.caribou.blade.api.response.DataSourceNameResponse;
import com.palantir.caribou.blade.api.response.DeleteFailure;
import com.palantir.caribou.blade.api.response.ObjectIdResponse;
import com.palantir.caribou.blade.api.response.Property;
import com.palantir.caribou.blade.api.response.PropertyResponse;
import com.palantir.caribou.blade.service.CaribouBladeClient;

import dao.AssassinDAL;
import jersey.repackaged.com.google.common.base.Preconditions;

public class Assassin {
	/**
	 * Logger.
	 */
	private static final Logger LOG = LoggerFactory.getLogger(Assassin.class); 
	
	
	
	/**
	 * Delete DAL.
	 */
	private final AssassinDAL assassinDAL;
	/**
	 * Caribou Blade Client.
	 */
	private final CaribouBladeClient bladeClient;
	/**
	 * Blade client username.
	 */
	private final String bladeUsername;
	/**
	 * Blade client password.
	 */
	private final String bladePassword;
	/**
	 * Batch size.
	 */
	private int batchSize;
	/**
	 * Search page size.
	 */
	private int pageSize;
	/**
	 * Search max number of results returned.
	 */
	private int maxSearchResults;
	
	/**
	 * Constructor.
	 * 
	 * @param assassinDAL
	 * 			The DeleteDAL to be used for all delete calls to the database.
	 * @param bladeClient
	 * 			The bladeClient to be used for all delete calls to palantir.
	 * @param bladeUsername
	 * 			The username that is passed into the blade client to create a request.
	 * @param bladePassword
	 * 			The password that is passed into the blade client to authenticate a request.
	 * @param maxSearchResults
	 * 			The maximum number of results to be returned on a dsname search.
	 * @param batchSize
	 * 			The batch size of a request.
	 * @param pageSize
	 * 			The pageSize of a request.
	 */
	public Assassin(final AssassinDAL assassinDAL, final CaribouBladeClient bladeClient, final String bladeUsername, final String bladePassword, final int maxSearchResults, final int batchSize, final int pageSize) {
		this.assassinDAL = Preconditions.checkNotNull(assassinDAL);
		this.bladeClient = Preconditions.checkNotNull(bladeClient);
		this.bladeUsername = Preconditions.checkNotNull(bladeUsername);
		this.bladePassword = Preconditions.checkNotNull(bladePassword);
		this.maxSearchResults = maxSearchResults;
		if (batchSize == 0) {
			this.batchSize = 100;
		} else {
			this.batchSize = batchSize;
		}
		
		if (pageSize == 0) {
			this.pageSize = 100;
		} else {
			this.pageSize = pageSize;
		}
	}
	
	/**
	 * Given a set of data source ids and a Multimap of datasource names to data source ids, get the set of
	 * data source names tied to the set of id's. 
	 * 
	 * @param dataSourceIds
	 * 		Set of data source ids to get the data source names for.
	 * @param dataSourceNamesToIdsMap
	 * 		The map of data source names to ids.
	 * @return
	 * 		Set of a data source names corresponding to the set of datasource ids.
	 */
	protected Set<String> getDataSourceNamesFromIds(Set<Long> dataSourceIds, Map<String, Collection<Long>> dataSourceNamesToIdsMap) {
		Set<String> dataSourceNames = new HashSet<String>();
		if (dataSourceNamesToIdsMap != null) {
			for (Entry<String, Collection<Long>> dataSourceNameToIdEntry : dataSourceNamesToIdsMap.entrySet()) {
				Collection<Long> dataSourceId = dataSourceNameToIdEntry.getValue();
				if (dataSourceIds.containsAll(dataSourceId)) {
					dataSourceNames.add(dataSourceNameToIdEntry.getKey());
				}
			}
		}
		return dataSourceNames;
	}

	/**
	 * Get a datasource delete preview for the datasources that are to be deleted on the next run.
	 * 
	 * @return
	 * 			DataSourceNameResponse containing the results of a delete request without actually deleting.
	 * @throws SQLException
	 * 			If there is an error getting all of the datasourcenames to preview delete.
	 */
	protected DataSourceNameResponse performDataSourcePreview() throws SQLException {
		Set<String> setOfDataSourceNamesToPreview = assassinDAL.getAllDataSourceNamesToDelete();
		LOG.info("performDataSourcePreview: Set of datasource names to preview: {}", setOfDataSourceNamesToPreview);
		
		if((setOfDataSourceNamesToPreview != null) && (!setOfDataSourceNamesToPreview.isEmpty())) {
			return bladeClient.previewByDataSourceName(new DataSourceNameRequest(setOfDataSourceNamesToPreview, pageSize, maxSearchResults, batchSize, true, bladeUsername, bladePassword));
		}
		return null;
	}

	/**
	 * Perform data source hides.
	 * 
	 * @throws SQLException
	 * 		If there is an error getting all the data source names to hide.
	 */
	protected void performDataSourceHide() throws SQLException {
		Set<String> setOfDataSourceNamesToHide = assassinDAL.getAllDataSourceNamesToHide();
		Set<String> setOfDataSourceNamesHiddenSuccessfully = new HashSet<String>();
		LOG.info("performDataSourceHide: Set of datasource names to hide: {}", setOfDataSourceNamesToHide);
		
		if ((setOfDataSourceNamesToHide == null) || (setOfDataSourceNamesToHide.isEmpty())) {
			LOG.info("performDataSourceHide: No datasource names to hide. SetOfDataSourceNamesToHide is null or empty.");
			return;
		}
		
		DataSourceNameResponse result = bladeClient.previewByDataSourceName(new DataSourceNameRequest(setOfDataSourceNamesToHide, pageSize, maxSearchResults, batchSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performDataSourceHide: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
		LOG.debug("performDataSourceHide: Result - {}", result);
    	setOfDataSourceNamesHiddenSuccessfully.addAll(getDataSourceNamesFromIds(result.getResult().getDeletedDataSourceIds(), result.getDataSourceNameIdsAsMap()));
    	
    	for (Entry<DeleteFailure, Collection<String>> failure : result.getDataSourceNameFailuresAsMap().entrySet()) {
    		if (StringUtils.contains(failure.getKey().getType(), "DataSourceNameDoesNotExistException")) {
    			LOG.info("performDataSourceHide: DataSourceNameDoesNotExistException: DataSourceName {}", failure.getValue());
    			setOfDataSourceNamesHiddenSuccessfully.addAll(failure.getValue());
    		}
    	}
    	LOG.info("performDataSourceHide: Set of datasource names hidden successfully {}", setOfDataSourceNamesHiddenSuccessfully);
    	setOfDataSourceNamesToHide.removeAll(setOfDataSourceNamesHiddenSuccessfully);
    	LOG.info("performDataSourceHide: Set of datasource names unsuccessfully attempted to hide {}", setOfDataSourceNamesToHide);
    		    
		LOG.info("performDataSourceHide: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updateDataSourceHideStatus(setOfDataSourceNamesHiddenSuccessfully, true);
		LOG.info("performDataSourceHide: Updating HideStatusFlag in delete database to false. Datasource names that were unsuccessful in being hidden. Increment retry count.");
		assassinDAL.updateDataSourceHideStatus(setOfDataSourceNamesToHide, false);
	}

	/**
	 * Perform data source deletion.
	 * 
	 * @throws SQLException
	 * 			If there is an error getting the data source names to delete.
	 */
	protected void performDataSourceDeletion() throws SQLException {
		Set<String> setOfDataSourceNamesDeletedSuccessfully = new HashSet<String>();
		Set<String> setOfDataSourceNamesToDelete = assassinDAL.getAllDataSourceNamesToDelete();
		LOG.info("performDataSourceDeletion: Set of datasource names to delete: {}", setOfDataSourceNamesToDelete);
		
		if((setOfDataSourceNamesToDelete == null) || (setOfDataSourceNamesToDelete.isEmpty())) {
			LOG.info("performDataSourceDeletion: No datasource names to delete. SetOfDataSourceNamesToDelete is null or empty.");
			return;
		}
		
		DataSourceNameResponse result = bladeClient.previewByDataSourceName(new DataSourceNameRequest(setOfDataSourceNamesToDelete, pageSize, maxSearchResults, batchSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performDataSourceDeletion: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
    	LOG.debug("performDataSourceDeletion: Result - {}", result);
    	setOfDataSourceNamesDeletedSuccessfully.addAll(getDataSourceNamesFromIds(result.getResult().getDeletedDataSourceIds(), result.getDataSourceNameIdsAsMap()));
    	
    	for (Entry<DeleteFailure, Collection<String>> failure : result.getDataSourceNameFailuresAsMap().entrySet()) {
    		if (StringUtils.contains(failure.getKey().getType(), "DataSourceNameDoesNotExistException")) {
    			LOG.info("performDataSourceDeletion: DataSourceNameDoesNotExistException: DataSourceName {}", failure.getValue());
    			setOfDataSourceNamesDeletedSuccessfully.addAll(failure.getValue());
    		}
    	}
    	LOG.info("performDataSourceDeletion: Set of datasource names deleted successfully {}", setOfDataSourceNamesDeletedSuccessfully);
    	setOfDataSourceNamesToDelete.removeAll(setOfDataSourceNamesDeletedSuccessfully);
    	LOG.info("performDataSourceDeletion: Set of datasource names unsuccessfully attempted to delete {}", setOfDataSourceNamesToDelete);
    		    
		LOG.info("performDataSourceDeletion: Updating SuccessFlag in delete database to true.");
		assassinDAL.updateDataSourceDeleteStatus(setOfDataSourceNamesDeletedSuccessfully, true);
		LOG.info("performDataSourceDeletion: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updateDataSourceHideStatus(setOfDataSourceNamesDeletedSuccessfully, true);
		LOG.info("performDataSourceDeletion: Updating SuccessFlag in delete database to false. Datasource names that were unsuccessful in delete. Increment retry count.");
		assassinDAL.updateDataSourceDeleteStatus(setOfDataSourceNamesToDelete, false);
	}
	
	/**
	 * Perform object preview.
	 * 
	 * @return
	 * 			The delete result from the object preview.
	 * @throws SQLException
	 * 			If there is an error getting the object ids to delete.
	 */
	protected ObjectIdResponse performObjectPreview() throws SQLException {
		Set<Long> setOfObjectIdsToDelete = assassinDAL.getAllObjectIdsToDelete();
		LOG.info("performObjectPreview: Set of object ids to preview: {}", setOfObjectIdsToDelete);
		
		if ((setOfObjectIdsToDelete != null) && (!setOfObjectIdsToDelete.isEmpty())) {
			return bladeClient.previewByObjectId(new ObjectIdRequest(setOfObjectIdsToDelete, batchSize, pageSize, true, bladeUsername, bladePassword));
		}
		return null;
	}

	/**
	 * Perform object hide.
	 * 
	 * @throws SQLException
	 * 			If there is an error getting all the object ids to hide.
	 */
	protected void performObjectHide() throws SQLException {
		Set<Long> setOfObjectIdsHiddenSuccessfully = new HashSet<Long>();
		Set<Long> setOfObjectIdsToHide = assassinDAL.getAllObjectIdsToHide();
		LOG.info("performObjectHide: Set of object ids to hide: {}", setOfObjectIdsToHide);
		
		if ((setOfObjectIdsToHide == null) || (setOfObjectIdsToHide.isEmpty())) {
			LOG.info("performObjectHide: No object ids to hide. SetOfObjectIdsToHide is null or empty.");
			return;
		}
		
		ObjectIdResponse result = bladeClient.previewByObjectId(new ObjectIdRequest(setOfObjectIdsToHide, batchSize, pageSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performObjectHide: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
	    
    	LOG.debug("performObjectHide: Result - {}", result);
    	setOfObjectIdsHiddenSuccessfully.addAll(result.getResult().getDeletedObjectIds());
    	for (Entry<DeleteFailure, Collection<Long>> failure : result.getFailuresAsMap().entrySet()) {
    		if (StringUtils.contains(failure.getKey().getType(), "ObjectIdsDoNotExistException")) {
    			LOG.info("performObjectHide: ObjectIdsDoNotExistException: Object Ids {}", failure.getValue());
    			setOfObjectIdsHiddenSuccessfully.addAll(failure.getValue());
    		}
    	}
    	LOG.info("performObjectHide: Set of object ids hidden successfully: {}.", setOfObjectIdsHiddenSuccessfully);
    	setOfObjectIdsToHide.removeAll(setOfObjectIdsHiddenSuccessfully);
    	LOG.info("performObjectHide: Set of object ids unsuccessful in hide: {}.", setOfObjectIdsToHide);
	
		LOG.info("performObjectHide: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updateObjectHideStatus(setOfObjectIdsHiddenSuccessfully, true);
		LOG.info("performDataSourceDeletion: Updating HideStatusFlag in delete database to false. Object Ids that were unsuccessful in hiding. Increment retry count.");
		assassinDAL.updateObjectHideStatus(setOfObjectIdsToHide, false);
	}

	/**
	 * Perform object deletion.
	 * 
	 * @throws SQLException
	 * 			If there is an error getting all the object ids to delete.
	 */
	protected void performObjectDeletion() throws SQLException {
		Set<Long> setOfObjectIdsDeletedSuccessfully = new HashSet<Long>();
		Set<Long> setOfObjectIdsToDelete = assassinDAL.getAllObjectIdsToDelete();
		LOG.info("performObjectDeletion: Set of object ids to delete: {}", setOfObjectIdsToDelete);
		
		if ((setOfObjectIdsToDelete == null) || (setOfObjectIdsToDelete.isEmpty())) {
			LOG.info("performObjectDeletion: No object ids to delete. SetOfObjectIdsToDelete is null or empty.");
			return;
		}
		
		ObjectIdResponse result = bladeClient.previewByObjectId(new ObjectIdRequest(setOfObjectIdsToDelete, batchSize, pageSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performObjectDeletion: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
    	
		LOG.debug("performObjectDeletion: Result - {}", result);
    	setOfObjectIdsDeletedSuccessfully.addAll(result.getResult().getDeletedObjectIds());
    	
    	for (Entry<DeleteFailure, Collection<Long>> failure : result.getFailuresAsMap().entrySet()) {
    		if (StringUtils.contains(failure.getKey().getType(), "ObjectIdsDoNotExistException")) {
    			LOG.info("performObjectDeletion: ObjectIdsDoNotExistException: Object Ids {}", failure.getValue());
    			setOfObjectIdsDeletedSuccessfully.addAll(failure.getValue());
    		}
    	}
    	
    	LOG.info("performObjectDeletion: Set of object ids deleted successfully: {}.", setOfObjectIdsDeletedSuccessfully);
    	setOfObjectIdsToDelete.removeAll(setOfObjectIdsDeletedSuccessfully);
    	LOG.info("performObjectDeletion: Set of object ids unsuccessful in delete: {}.", setOfObjectIdsToDelete);
	    
		LOG.info("performObjectDeletion: Updating SuccessFlag in delete database to true.");
		assassinDAL.updateObjectDeleteStatus(setOfObjectIdsDeletedSuccessfully, true);
		LOG.info("performObjectDeletion: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updateObjectHideStatus(setOfObjectIdsDeletedSuccessfully, true);
		LOG.info("performObjectDeletion: Updating SuccessFlag in delete database to false. Object ids that were unsuccessful in delete. Increment retry count.");
		assassinDAL.updateObjectDeleteStatus(setOfObjectIdsToDelete, false);
	}
	
	/**
	 * Perform property preview.
	 * 
	 * @return
	 * 			The delete result of a property preview.
	 * @throws SQLException
	 * 			If there is an error getting all the properties to delete.
	 */
	protected PropertyResponse performPropertyPreview() throws SQLException {
		SetMultimap<String, String> propertyUriAndValuesToDelete = assassinDAL.getAllPropertiesToDelete();
		LOG.info("performPropertyPreview: Set of Property uris and values to preview: {}", propertyUriAndValuesToDelete);
		
		if ((propertyUriAndValuesToDelete != null) && (!propertyUriAndValuesToDelete.isEmpty())) {
		    return bladeClient.previewByProperty(new PropertyRequest(propertyUriAndValuesToDelete, pageSize, batchSize, true, bladeUsername, bladePassword));
		}
		return null;
	}
	
	/**
	 * Perform property hides.
	 * 
	 * @throws SQLException
	 * 		If there is an error getting all of the properties that are to be hidden.
	 */
	protected void performPropertyHide() throws SQLException {
		SetMultimap<String, String> propertyUriAndValuesToHide = assassinDAL.getAllPropertiesToDelete();
		SetMultimap<String, String> unsuccessfulPropertyDeletes = HashMultimap.create();
		LOG.info("performPropertyHide: Set of Property uris and values to delete: {}", propertyUriAndValuesToHide);
		if ((propertyUriAndValuesToHide == null) || (propertyUriAndValuesToHide.isEmpty())) {
			LOG.info("performPropertyHide: No properties to hide. PropertyUriAndValuesToDelete is null or empty.");
			return;
		}
		
		PropertyResponse result = bladeClient.previewByProperty(new PropertyRequest(propertyUriAndValuesToHide, pageSize, batchSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performPropertyHide: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
		LOG.debug("performPropertyHide: Result - {}", result);
	    	
    	for (Entry<DeleteFailure, Collection<Property>> propertyDeleteFailure : result.getPropertyFailuresAsMap().entrySet()) {
    		for (Property property : propertyDeleteFailure.getValue()) {
    			propertyUriAndValuesToHide.remove(property.getType(), property.getValue());
    			unsuccessfulPropertyDeletes.put(property.getType(), property.getValue());
    		}
    	}
    	
    	LOG.info("performPropertyHide: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updatePropertyHideStatus(propertyUriAndValuesToHide.asMap(), true);
		LOG.info("performPropertyHide: Updating SuccessFlag in delete database to false. For unsuccessful property updates.");
		assassinDAL.updatePropertyHideStatus(unsuccessfulPropertyDeletes.asMap(), false);
	}
	
	/**
	 * Perform property deletion.
	 * 
	 * @throws SQLException
	 * 			If there is an error getting all of the properties to delete.
	 */
	protected void performPropertyDeletion() throws SQLException {
		SetMultimap<String, String> propertyUriAndValuesToDelete = assassinDAL.getAllPropertiesToDelete();
		LOG.info("performPropertyDeletion: Set of Property uris and values to delete: {}", propertyUriAndValuesToDelete);
		if ((propertyUriAndValuesToDelete == null) || (propertyUriAndValuesToDelete.isEmpty())) {
			LOG.info("performPropertyDeletion: No properties to hide. PropertyUriAndValuesToDelete is null or empty.");
			return;
		}
		
		SetMultimap<String, String> unsuccessfulPropertyDeletes = HashMultimap.create();
		PropertyResponse result = bladeClient.previewByProperty(new PropertyRequest(propertyUriAndValuesToDelete, pageSize, batchSize, false, bladeUsername, bladePassword));
	    if ((result == null) || (result.getResult() == null)) {
	    	LOG.error("performPropertyDeletion: DataSourceNameResponse or DeleteResult is null.");
	    	return;
	    }
		
	    LOG.debug("performPropertyDeletion: Result - {}", result);
    	for (Entry<DeleteFailure, Collection<Property>> propertyDeleteFailure : result.getPropertyFailuresAsMap().entrySet()) {
    		for (Property property : propertyDeleteFailure.getValue()) {
    			propertyUriAndValuesToDelete.remove(property.getType(), property.getValue());
    			unsuccessfulPropertyDeletes.put(property.getType(), property.getValue());
    		}
    	}
    	
    	LOG.info("performPropertyDeletion: Updating SuccessFlag in delete database to true.");
		assassinDAL.updatePropertyDeleteStatus(propertyUriAndValuesToDelete.asMap(), true);
		LOG.info("performPropertyDeletion: Updating HideStatusFlag in delete database to true.");
		assassinDAL.updatePropertyHideStatus(propertyUriAndValuesToDelete.asMap(), true);
		LOG.info("performPropertyDeletion: Updating SuccessFlag in delete database to false. For unsuccessful property updates.");
		assassinDAL.updatePropertyDeleteStatus(unsuccessfulPropertyDeletes.asMap(), false);
	}
}

package processor;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.palantir.caribou.blade.api.response.DataSourceNameResponse;
import com.palantir.caribou.blade.api.response.ObjectIdResponse;
import com.palantir.caribou.blade.api.response.PropertyResponse;
import com.palantir.caribou.blade.service.CaribouBladeClient;

import dao.AssassinDAL;
import dao.AssassinDALImpl;

/**
 * Delete Processor Client.
 * 
 * @author pkandasamy
 */
public class AssassinClient {
	/** 
	 * Logger. 
	 */
	private static Logger LOG = LoggerFactory.getLogger(AssassinClient.class);
	
	public static void main(String[] args) {
		AssassinDAL assassinDAL = null;
		CaribouBladeClient bladeClient = null;
		try {
			Options options = new Options();
			Option configOption = new Option("c", "properties", true, "Properties File Path");
			configOption.hasArgs();
			configOption.setRequired(true);
			options.addOption(configOption);
			Option previewOption = new Option("p", "Preview Statistics");
			options.addOption(previewOption);
			Option hideOption = new Option("h", "Hide");
			options.addOption(hideOption);
			Option deleteOption = new Option("d", "Delete");
			options.addOption(deleteOption);
			
			CommandLineParser parser = new DefaultParser();
			CommandLine cmd = parser.parse(options, args);
			
			Properties properties = getProperties(cmd.getOptionValues("properties")[0]);
			String bladeUsername = properties.getProperty(AssassinConstants.Properties.BLADE_USERNAME);
			String bladePassword = properties.getProperty(AssassinConstants.Properties.BLADE_PASSWORD);
			int batchSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_BATCH_SIZE, "100"));
			int pageSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_PAGE_SIZE, "100"));
			int maxSearchResults = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_MAX_SEARCH_RESULTS, "0"));
			
			assassinDAL = initializeDeleteDAL(properties);
			bladeClient = initializeCaribouBladeClient(properties);
			Assassin deleteProcessor = new Assassin(assassinDAL, bladeClient, bladeUsername, bladePassword, maxSearchResults, batchSize, pageSize);
			
			if (cmd.hasOption("h")) {
				deleteProcessor.performObjectHide();
				deleteProcessor.performDataSourceHide();
				deleteProcessor.performPropertyHide();
			} else if (cmd.hasOption("d")) {
				deleteProcessor.performObjectDeletion();
				deleteProcessor.performDataSourceDeletion();
				deleteProcessor.performPropertyDeletion();
			} else if (cmd.hasOption("p")) {
				DataSourceNameResponse dsResponse = deleteProcessor.performDataSourcePreview();
				ObjectIdResponse objResponse = deleteProcessor.performObjectPreview();
				PropertyResponse propResponse = deleteProcessor.performPropertyPreview();
				
				if (dsResponse != null) {
					System.out.println("DataSource Statistics: " + dsResponse.getObjectStats());
					System.out.println("DataSource Failures: " + dsResponse.getDataSourceIdFailuresAsMap());
				}
				if (objResponse != null) {
					System.out.println("Object Statistics: " + objResponse.getObjectStats());
					System.out.println("Object Failures: " + objResponse.getFailuresAsMap());
				}
				if (propResponse != null) {
					System.out.println("Property Statistics: " + propResponse.getObjectStats());
					System.out.println("Property Failures: " + propResponse.getPropertyFailuresAsMap());
				}
			}
		} catch (SQLException e) {
			LOG.error("Unable to run delete processor client.", e);
		} catch (ParseException e) {
			LOG.error("Unable to parse command line arguments.", e);
		} catch (GeneralSecurityException e) {
			LOG.error("Unable to initialize blade client.", e);
		} catch (IOException e) {
			LOG.error("Unable to create properties file.", e);
		} finally {
			IOUtils.closeQuietly(assassinDAL);
			IOUtils.closeQuietly(bladeClient);
		}
	}
	
	public static AssassinDAL initializeDeleteDAL(Properties properties) throws SQLException, IOException {
		return new AssassinDALImpl(properties);
	}
	
	public static CaribouBladeClient initializeCaribouBladeClient(Properties properties) throws GeneralSecurityException {
		String serverName = properties.getProperty(AssassinConstants.Properties.BLADE_SERVER_NAME);
		int port = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_PORT));
		boolean useSsl = Boolean.parseBoolean(properties.getProperty(AssassinConstants.Properties.BLADE_USE_SSL));
		return new CaribouBladeClient(serverName, port, useSsl);
	}
	
	/**
	 * Get the properties from the config file.
	 * 
	 * @param fileName
	 * 		The filename.
	 * @return
	 * 		The properties from the config file.
	 * @throws IOException 
	 */
	public static Properties getProperties(String fileName) throws IOException {
		Properties properties = new Properties();
		File propertiesFile = new File(fileName);
		
		try (FileInputStream propertiesStream = new FileInputStream(propertiesFile)) {
			properties.load(propertiesStream);
		} 
		return properties;
	}
	
	private AssassinClient() {
		
	}
}

package processor;

/**
 * Constants used by the delete processor.
 * 
 * @author pkandasamy
 *
 */
public final class AssassinConstants {
	/**
	 * Constants from the properties config file.
	 * 
	 * @author pkandasamy
	 *
	 */
	public final class Properties {
		/**
		 * Blade Client server name from properties file.
		 */
		public static final String BLADE_SERVER_NAME = "bladeclient.servername";
		/**
		 * Blade Client port from properties file.
		 */
		public static final String BLADE_PORT = "bladeclient.port";
		public static final String BLADE_USE_SSL = "bladeclient.usessl";
		public static final String BLADE_USERNAME = "bladeclient.username";
		public static final String BLADE_PASSWORD = "bladeclient.encryptedpassword";
		public static final String BLADE_BATCH_SIZE = "bladeclient.batchsize";
		public static final String BLADE_PAGE_SIZE = "bladeclient.pagesize";
		public static final String BLADE_MAX_SEARCH_RESULTS = "bladeclient.maxsearchresults";
		
		
		/**
		 * Private constructor to prevent instantiation.
		 */
		private Properties() {
		}
	}
	
	private AssassinConstants() {
		
	}
}

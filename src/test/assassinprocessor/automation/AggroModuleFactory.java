/*
 * Copyright 2015 Palantir Technologies, Inc. All rights reserved.
 */

package automation;

import static com.google.common.base.Preconditions.checkArgument;

import java.io.File;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.palantir.service.prefs.AggroPrefs;
import com.palantir.service.prefs.PropertyProviderBackedAggroPrefs;
import com.palantir.service.services.AggroModule;
import com.palantir.service.services.AggroModules;

public class AggroModuleFactory {
    private static final String SERVER_ADDRESS = "SERVER_ADDRESS";
    private static final String SERVER_PORT = "SERVER_PORT";
    private static final String USE_SSL = "USE_SSL";
    private static final String SECURE_SERVER_PASSWORD = "SECURE_SERVER_PASSWORD";
    private static final String URL_TAIL_PREFIX = "URL_TAIL_PREFIX";
    private static final String DEFAULT_URL_TAIL_PREFIX = "/services/services/";

    private final String host;
    private final String port;
    private final String useSsl;
    private final String secureServerPassword;
    private final String trustStore;

    @JsonCreator
    public AggroModuleFactory(
            @JsonProperty("host") String host,
            @JsonProperty("port") String port,
            @JsonProperty("useSsl") String useSsl,
            @JsonProperty("secureServerPassword") String secureServerPassword,
            @JsonProperty("trustStore") String trustStore) {
        checkArgument(StringUtils.isNotBlank(host), "Aggro host cannot be blank.");
        checkArgument(StringUtils.isNotBlank(port), "Aggro port cannot be blank.");
        checkArgument(StringUtils.isNotBlank(useSsl), "Aggro useSsl cannot be blank.");
        checkArgument(StringUtils.isNotBlank(secureServerPassword),
                "Aggro secureServerPassword cannot be blank.");
        checkArgument(StringUtils.isNotBlank(secureServerPassword),
                "Aggro secureServerPassword cannot be blank.");
        checkArgument(StringUtils.isNotBlank(trustStore), "Aggro trustStore cannot be blank.");
        checkArgument(new File(trustStore).exists(), "Path to trustStore is invalid.");

        System.setProperty("javax.net.ssl.trustStore", trustStore);

        this.host = host;
        this.port = port;
        this.useSsl = useSsl;
        this.secureServerPassword = secureServerPassword;
        this.trustStore = trustStore;
    }

    @JsonIgnore
    public AggroModule getAggroModule() {
        return AggroModules.createModule(getAggroPrefs());
    }

    @JsonIgnore
    private AggroPrefs getAggroPrefs() {
        return new PropertyProviderBackedAggroPrefs(this::getAggroProperties);
    }

    @JsonProperty
    public String getHost() {
        return host;
    }

    @JsonProperty
    public String getPort() {
        return port;
    }

    @JsonProperty
    public String getUseSsl() {
        return useSsl;
    }

    @JsonProperty
    public String getSecureServerPassword() {
        return secureServerPassword;
    }

    @JsonProperty
    public String getTrustStore() {
        return trustStore;
    }

    @JsonIgnore
    public Properties getAggroProperties() {
        Properties props = new Properties(PropertyProviderBackedAggroPrefs.DEFAULTS);
        props.setProperty(SERVER_ADDRESS, host);
        props.setProperty(SERVER_PORT, port);
        props.setProperty(USE_SSL, useSsl);
        props.setProperty(SECURE_SERVER_PASSWORD, secureServerPassword);
        props.setProperty(URL_TAIL_PREFIX, DEFAULT_URL_TAIL_PREFIX);
        return props;
    }
}

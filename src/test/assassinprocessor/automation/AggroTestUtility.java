package automation;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.palantir.caribou.blade.service.CaribouBladeClient;
import com.palantir.model.Role;
import com.palantir.model.datasource.DataSource;
import com.palantir.model.impl.datasource.SimpleDataSourceRecord;
import com.palantir.model.impl.property.SimpleSimplePropertyValue;
import com.palantir.model.session.UserSessionToken;
import com.palantir.service.datasource.CreateDataSource;
import com.palantir.service.datasource.DataSourceSearchResult;
import com.palantir.service.impl.datasource.SimpleDataSourceSearch;
import com.palantir.service.impl.datasource.builder.CreateDataSourceBuilder;
import com.palantir.service.impl.login.SimplePasswordLoginRequest;
import com.palantir.service.impl.object.write.builder.CreateObjectBuilder;
import com.palantir.service.impl.object.write.builder.CreatePropertyBuilder;
import com.palantir.service.object.write.CreateObject;
import com.palantir.service.services.AggroModule;

import dao.AssassinDAL;
import processor.Assassin;
import processor.AssassinClient;

public class AggroTestUtility {
	private static final String aggroUserName = "piradeep";
	private static final String aggroPassword = "Password1!";
	private static final String aggroSecureServer = "40LEhqMuygcAqUIR4LxbyeJfogHKOmHATYOwM7erzGA=";
	private static final String aggroTrustStore = "security/Client_Truststore";
	private static final String aggroHost = "ec2-52-3-110-120.compute-1.amazonaws.com";
	private static final int aggroPort = 3280;
	private static final String aggroUseSsl = "true";
	private AggroModuleFactory amf;
	
	Assassin assassinator;
	AssassinDAL assassinDAL;
	CaribouBladeClient bladeClient;
	
	@BeforeClass
	public void setUp() throws IOException, SQLException {
		amf = new AggroModuleFactory(aggroHost, Integer.toString(aggroPort), aggroUseSsl, aggroSecureServer, aggroTrustStore);
		Properties properties = AssassinClient.getProperties("config.properties");
		assassinDAL = AssassinClient.initializeDeleteDAL(properties);
	}
	
	@Test
	public void test() {
		AggroModule aModule = amf.getAggroModule();
		UserSessionToken userSession = aModule.getLoginService().loginCreateNewSession(new SimplePasswordLoginRequest("piradeep", "Password1!"));

		for (int count = 1; count <= 100; count += 1) {
			String fileName = "Assassin" + count + ".txt";
			CreateDataSource dataSourceCreated = 
					new CreateDataSourceBuilder().description(fileName).name(fileName).aclId(2L).build();
			Long dataSourceId = aModule.getDataSourceService().createDataSource(userSession, 1L, dataSourceCreated);
			
			CreateObject createObj = new CreateObjectBuilder()
					.objectTypeUri("com.palantir.object.document")
					.addInitialProperty(new CreatePropertyBuilder()
							.type("com.palantir.property.Name")
							.value(new SimpleSimplePropertyValue("Assassinate Delete Requests"))
							.role(Role.NONE)
							.dataSourceRecords(Collections.singleton(new SimpleDataSourceRecord(dataSourceId, 2L)))
							.build()).build();
			aModule.getObjectWriteService().createObjects(userSession, 1L, Lists.newArrayList(createObj));
		}
		
		
	}
	
	public void testDataSourcePreview() {
		
	}

}

package dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.palantir.caribou.blade.service.CaribouBladeClient;
import com.palantir.model.Role;
import com.palantir.model.impl.datasource.SimpleDataSourceRecord;
import com.palantir.model.impl.property.SimpleSimplePropertyValue;
import com.palantir.model.session.UserSessionToken;
import com.palantir.service.datasource.CreateDataSource;
import com.palantir.service.impl.datasource.builder.CreateDataSourceBuilder;
import com.palantir.service.impl.login.SimplePasswordLoginRequest;
import com.palantir.service.impl.object.write.builder.CreateObjectBuilder;
import com.palantir.service.impl.object.write.builder.CreatePropertyBuilder;
import com.palantir.service.object.write.CreateObject;
import com.palantir.service.object.write.CreateObjectsResult;
import com.palantir.service.services.AggroModule;

import automation.AggroModuleFactory;
import processor.Assassin;
import processor.AssassinClient;
import processor.AssassinConstants;

public class AssassinDALTest {
	private static final String aggroUserName = "piradeep";
	private static final String aggroPassword = "Password1!";
	private static final String aggroSecureServer = "40LEhqMuygcAqUIR4LxbyeJfogHKOmHATYOwM7erzGA=";
	private static final String aggroTrustStore = "security/Client_Truststore";
	private static final String aggroHost = "ec2-52-3-110-120.compute-1.amazonaws.com";
	private static final int aggroPort = 3280;
	private static final String aggroUseSsl = "true";
	private AggroModuleFactory amf;
	
	Assassin assassin;
	AssassinDAL assassinDAL;
	CaribouBladeClient bladeClient;
	
	@BeforeClass
	public void setUp() throws IOException {
		try {
			Properties properties = AssassinClient.getProperties("config.properties");
			String bladeUsername = properties.getProperty(AssassinConstants.Properties.BLADE_USERNAME);
			String bladePassword = properties.getProperty(AssassinConstants.Properties.BLADE_PASSWORD);
			int batchSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_BATCH_SIZE, "100"));
			int pageSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_PAGE_SIZE, "100"));
			int maxSearchResults = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_MAX_SEARCH_RESULTS, "0"));
			
			amf = new AggroModuleFactory(aggroHost, Integer.toString(aggroPort), aggroUseSsl, aggroSecureServer, aggroTrustStore);
			assassinDAL = AssassinClient.initializeDeleteDAL(properties);
			bladeClient = AssassinClient.initializeCaribouBladeClient(properties);
			assassin = new Assassin(assassinDAL, bladeClient, bladeUsername, bladePassword, maxSearchResults, batchSize, pageSize);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (GeneralSecurityException e) {
			e.printStackTrace();
		}
	}
	
	public void populateDataSources() throws SQLException {
		AggroModule aModule = amf.getAggroModule();
		UserSessionToken userSession = aModule.getLoginService().loginCreateNewSession(new SimplePasswordLoginRequest(aggroUserName, aggroPassword));

		for (int dsCount = 1; dsCount <= 100; dsCount += 1) {
			String fileName = "Assassin" + dsCount + ".txt";
			CreateDataSource dataSourceCreated = 
					new CreateDataSourceBuilder().description(fileName).name(fileName).aclId(2L).build();
			Long dataSourceId = aModule.getDataSourceService().createDataSource(userSession, 1L, dataSourceCreated);
			assassinDAL.createDataSourceSubmission(fileName, "NINJASSIN");
			
			for (int objCount = 1; objCount <= 5; objCount += 1) {
				CreateObject createObj = new CreateObjectBuilder()
						.objectTypeUri("com.palantir.object.document")
						.addInitialProperty(new CreatePropertyBuilder()
								.type("com.palantir.property.Name")
								.value(new SimpleSimplePropertyValue("Assassinate Delete Document X" + objCount))
								.role(Role.NONE)
								.dataSourceRecords(Collections.singleton(new SimpleDataSourceRecord(dataSourceId, 2L)))
								.build()).build();
				CreateObjectsResult objResult = aModule.getObjectWriteService().createObjects(userSession, 1L, Lists.newArrayList(createObj));
				
				for (Long objectId : objResult.getCreatedObjectIds()) {
					assassinDAL.createObjectSubmission(objectId, "NINJASSIN");
				}
			}
		}
	}
	
	@Test
	public void testGetAllDsNamesToDelete() throws SQLException {
		Set<String> dataSourceNamesToDelete = assassinDAL.getAllDataSourceNamesToDelete();
		assertNotNull("Set of data source names to delete is null.", dataSourceNamesToDelete);
		assertTrue("Data source names to delete are 0.", dataSourceNamesToDelete.isEmpty());
		for (String dataSourceName : dataSourceNamesToDelete) {
			assertTrue("Data source name is null or empty.", StringUtils.isNotBlank(dataSourceName));
		}
	}
	
	@Test
	public void testGetAllDsNamesToHide() throws SQLException {
		Set<String> dataSourceNamesToHide = assassinDAL.getAllDataSourceNamesToHide();
		assertNotNull("Set of data source names to hide is null.", dataSourceNamesToHide);
		assertTrue("Data source names to hide are 0.", dataSourceNamesToHide.isEmpty());
		for (String dataSourceName : dataSourceNamesToHide) {
			assertTrue("Data source name is null or empty.", StringUtils.isNotBlank(dataSourceName));
		}
	}
	
	@Test
	public void testGetAllDsDeleteSubmissionsToDelete() throws SQLException {
		Set<DataSourceDeleteSubmission> dsSubmissionsToDelete = assassinDAL.getAllDSDeleteSubmissionsToDelete();
		assertNotNull("Data source submissions to delete is null", dsSubmissionsToDelete);
		assertTrue("Size of data source names to delete is 0.", dsSubmissionsToDelete.isEmpty());
		
		for (DataSourceDeleteSubmission dsSubmission : dsSubmissionsToDelete) {
			assertTrue("Success flag should be false.", !dsSubmission.getSuccessFlag());
			assertNotNull("Retry count is null.", dsSubmission.getRetryCount());
			assertTrue("Data source name is null or empty.", StringUtils.isNotBlank(dsSubmission.getDsName()));
			assertTrue("Source system name is null or empty.", StringUtils.isNotBlank(dsSubmission.getSourceSystemName()));
			assertNotNull("DsSubmissionId is null.", dsSubmission.getDsDeleteSubmissionId());
		}
	}
	
	@Test
	public void testGetAllDsDeleteSubmissionsToHide() throws SQLException {
		Set<DataSourceDeleteSubmission> dsSubmissionsToHide = assassinDAL.getAllDSDeleteSubmissionsToHide();
		assertNotNull("Set of data source submissions to hide is null.", dsSubmissionsToHide);
		assertTrue("Size of data source names to hide is 0.", dsSubmissionsToHide.isEmpty());
		
		for (DataSourceDeleteSubmission dsSubmission : dsSubmissionsToHide) {
			assertTrue("Success flag should be false.", !dsSubmission.getSuccessFlag());
			assertNotNull("Retry count is null.", dsSubmission.getRetryCount());
			assertTrue("Data source name is null or empty.", StringUtils.isNotBlank(dsSubmission.getDsName()));
			assertTrue("Source system name is null or empty.", StringUtils.isNotBlank(dsSubmission.getSourceSystemName()));
			assertNotNull("DsSubmissionId is null.", dsSubmission.getDsDeleteSubmissionId());
		}
	}

	@Test 
	public void testGetAllObjectIdsToDelete() throws SQLException {
		Set<Long> objectIdsToDelete = assassinDAL.getAllObjectIdsToDelete();
		assertNotNull("Set of object Ids to delete is null.", objectIdsToDelete);
		assertTrue("Size of object ids to delete is 0.", objectIdsToDelete.isEmpty());
	}
}

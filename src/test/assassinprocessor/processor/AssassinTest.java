package processor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.palantir.caribou.blade.api.response.DataSourceNameResponse;
import com.palantir.caribou.blade.api.response.ObjectIdResponse;
import com.palantir.caribou.blade.api.response.PropertyResponse;
import com.palantir.caribou.blade.service.CaribouBladeClient;
import com.palantir.model.Role;
import com.palantir.model.impl.datasource.SimpleDataSourceRecord;
import com.palantir.model.impl.property.SimpleSimplePropertyValue;
import com.palantir.model.session.UserSessionToken;
import com.palantir.service.datasource.CreateDataSource;
import com.palantir.service.impl.datasource.builder.CreateDataSourceBuilder;
import com.palantir.service.impl.login.SimplePasswordLoginRequest;
import com.palantir.service.impl.object.write.builder.CreateObjectBuilder;
import com.palantir.service.impl.object.write.builder.CreatePropertyBuilder;
import com.palantir.service.object.write.CreateObject;
import com.palantir.service.object.write.CreateObjectsResult;
import com.palantir.service.services.AggroModule;

import automation.AggroModuleFactory;
import dao.AssassinDAL;

public class AssassinTest {
	private static final String aggroUserName = "piradeep";
	private static final String aggroPassword = "Password1!";
	private static final String aggroSecureServer = "40LEhqMuygcAqUIR4LxbyeJfogHKOmHATYOwM7erzGA=";
	private static final String aggroTrustStore = "security/Client_Truststore";
	private static final String aggroHost = "ec2-52-3-110-120.compute-1.amazonaws.com";
	private static final int aggroPort = 3280;
	private static final String aggroUseSsl = "true";
	private AggroModuleFactory amf;
	
	Assassin assassin;
	AssassinDAL assassinDAL;
	CaribouBladeClient bladeClient;
	Properties properties;
	
	@Before
	public void setUp() throws SQLException, IOException, GeneralSecurityException {
		Properties properties = AssassinClient.getProperties("config.properties");
		String bladeUsername = properties.getProperty(AssassinConstants.Properties.BLADE_USERNAME);
		String bladePassword = properties.getProperty(AssassinConstants.Properties.BLADE_PASSWORD);
		int batchSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_BATCH_SIZE, "100"));
		int pageSize = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_PAGE_SIZE, "100"));
		int maxSearchResults = Integer.parseInt(properties.getProperty(AssassinConstants.Properties.BLADE_MAX_SEARCH_RESULTS, "100"));
		
		amf = new AggroModuleFactory(aggroHost, Integer.toString(aggroPort), aggroUseSsl, aggroSecureServer, aggroTrustStore);
		assassinDAL = AssassinClient.initializeDeleteDAL(properties);
		bladeClient = AssassinClient.initializeCaribouBladeClient(properties);
		assassin = new Assassin(assassinDAL, bladeClient, bladeUsername, bladePassword, maxSearchResults, batchSize, pageSize);
		
//		MockConnection mockConnection = new MockConnection();
	}	

//	@Test 
//	public void testMain() {
//		String[] args = {"-c", "config.properties", "-p"};
//		System.out.println(args[0]);
//		AssassinClient.main(args);
//	}
	
	@Test
	public void testGetDataSourceNamesFromIds() {
		Map<String, Collection<Long>> dataSourceNamesToIdMapping = new HashMap<String, Collection<Long>>();
		dataSourceNamesToIdMapping.put("Data Source A", Collections.singleton(1L));
		dataSourceNamesToIdMapping.put("Data Source B", Collections.singleton(2L));
		dataSourceNamesToIdMapping.put("Data Source C", Collections.singleton(3L));
		
		Set<Long> dataSourceIds = new HashSet<Long>();
		dataSourceIds.add(1L);
		Set<String> dataSourceNames = assassin.getDataSourceNamesFromIds(dataSourceIds, dataSourceNamesToIdMapping);
		assertNotNull("Data source names from id is null.", dataSourceNames);
		assertEquals("More than 1 data source name found for id.", dataSourceNames.size(), 1);
		for (String dsName : dataSourceNames) {
			assertEquals("Incorrect data source name tied to id.", "Data Source A", dsName);
		}
		
		dataSourceIds.clear();
		dataSourceNames.clear();
		assertEquals("Data source ids did not clear.", dataSourceIds.size(), 0);
		assertEquals("Data source names did not clear.", dataSourceNames.size(), 0);
		
		dataSourceIds.add(2L);
		dataSourceIds.add(3L);
		dataSourceNames = assassin.getDataSourceNamesFromIds(dataSourceIds, dataSourceNamesToIdMapping);
		assertNotNull("Data source names from id is null.", dataSourceNames);
		assertEquals("More than 2 data source name found for id.", dataSourceNames.size(), 2);
		assertTrue("Incorrect data source name tied to id.", dataSourceNames.contains("Data Source B"));
		assertTrue("Incorrect data source name tied to id.", dataSourceNames.contains("Data Source C"));
	}
	
	@Test
	public void testDatasourcePreview() throws SQLException {
		populateDataSources();
		DataSourceNameResponse dsResponse = assassin.performDataSourcePreview();
		if (dsResponse != null) {
			System.out.println("DataSource Statistics: " + dsResponse.getObjectStats());
			System.out.println("DataSource Id Failures: " + dsResponse.getDataSourceIdFailuresAsMap());
			System.out.println("DataSource Name Failures: " + dsResponse.getDataSourceNameFailuresAsMap());
		}
	}
	
	public void populateDataSources() throws SQLException {
		AggroModule aModule = amf.getAggroModule();
		UserSessionToken userSession = aModule.getLoginService().loginCreateNewSession(new SimplePasswordLoginRequest(aggroUserName, aggroPassword));

		for (int dsCount = 1; dsCount <= 100; dsCount += 1) {
			String fileName = "Assassin" + dsCount + ".txt";
			CreateDataSource dataSourceCreated = 
					new CreateDataSourceBuilder().description(fileName).name(fileName).aclId(2L).build();
			Long dataSourceId = aModule.getDataSourceService().createDataSource(userSession, 1L, dataSourceCreated);
			assassinDAL.createDataSourceSubmission(fileName, "NINJASSIN");
			
			for (int objCount = 1; objCount <= 5; objCount += 1) {
				CreateObject createObj = new CreateObjectBuilder()
						.objectTypeUri("com.palantir.object.document")
						.addInitialProperty(new CreatePropertyBuilder()
								.type("com.palantir.property.Name")
								.value(new SimpleSimplePropertyValue("Assassinate Delete Document X" + objCount))
								.role(Role.NONE)
								.dataSourceRecords(Collections.singleton(new SimpleDataSourceRecord(dataSourceId, 2L)))
								.build()).build();
				CreateObjectsResult objResult = aModule.getObjectWriteService().createObjects(userSession, 1L, Lists.newArrayList(createObj));
				
				for (Long objectId : objResult.getCreatedObjectIds()) {
					assassinDAL.createObjectSubmission(objectId, "NINJASSIN");
				}
			}
		}
	}
	
	@Test
	public void testObjectPreview() throws SQLException {
		ObjectIdResponse objResponse = assassin.performObjectPreview();
		if (objResponse != null) {
			System.out.println("Object Statistics: " + objResponse.getObjectStats());
			System.out.println("Object Failures: " + objResponse.getFailuresAsMap());
		}
	}
	
	@Test
	public void testPropertyPreview() throws SQLException {
		PropertyResponse propResponse = assassin.performPropertyPreview();
		if (propResponse != null) {
			System.out.println("Property Statistics: " + propResponse.getObjectStats());
			System.out.println("Property Failures: " + propResponse.getPropertyFailuresAsMap());
		}
	}
	
	@After
	public void tearDown() {
		IOUtils.closeQuietly(assassinDAL);
		IOUtils.closeQuietly(bladeClient);
	}
}
